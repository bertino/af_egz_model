TARGET   = af_egz_model

SOURCES := $(wildcard src/*.cpp)
OBJECTS := $(patsubst src/%.cpp,obj/%.o,$(SOURCES))
DEPENDS := $(patsubst src/%.cpp,obj/%.d,$(SOURCES))

CXXFLAGS = -g -I. -std=c++17

# ADD MORE WARNINGS!
WARNING := -Wall -Wextra

# .PHONY means these rules get executed even if
# files of those names exist.
.PHONY: all clean

# The first rule is the default, ie. "make",
# "make all" and "make parking" mean the same
all: bin/$(TARGET)

clean:
	$(RM) $(OBJECTS) $(DEPENDS) $(TARGET)

# Linking the executable from the object files
bin/$(TARGET): $(OBJECTS)
	@echo "target : $(TARGET)"
	@echo "sources: $(SOURCES)"
	@echo "objects: $(OBJECTS)"
	@echo "depends: $(DEPENDS)"
	@mkdir -p bin
	$(CXX) $(WARNING) $(CXXFLAGS) $^ -o $@

-include $(DEPENDS)

obj/%.o: src/%.cpp makefile
	@mkdir -p obj
	$(CXX) $(WARNING) $(CXXFLAGS) -MMD -MP -c $< -o $@
