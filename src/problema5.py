import sys


def from_base_10(number: int, base: int):
    result = []
    while number > 0:
        result.append(number % base)
        number = int(number / base)
    result.reverse()
    return result


def is_magic(x: int, p: int, q: int):
    return set(from_base_10(x, p)) == set(from_base_10(x, q))


def main(n=0, p=10, q=5):
    magic_numbers = []
    for x in range(1, n):
        if is_magic(x, p, q):
            magic_numbers.append(x)
    print(magic_numbers)


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: problema5.py n p q")
    main(n=int(sys.argv[1]), p=int(sys.argv[2]), q=int(sys.argv[3]))
