#include "problema2.hpp"
#include "problema6.hpp"
#include <string>

void problema2()
{
    Stack<int> stack;
    int n;
    std::cin >> n;

    do {
        stack.push(n % 10);
        n /= 10;
    } while (n);
    n = stack.pop();
    while (n != -1) {
        std::cout << n;
        n = stack.pop();
    }
}

int main(int argc, char** argv)
{
    if (argc == 1) {
        std::cout << "Specificati problema de rezolvat ca parametru" << std::endl;
        return 1;
    }
    int problema;
    try {
        problema = std::stoi(argv[1]);
    } catch (std::invalid_argument) {
        std::cout << "Specificati problema de rezolvat ca parametru" << std::endl;
        return 1;
    }
    switch (problema) {
    case 2:
        problema2();
        break;
    case 6:
        Solver solver;
        auto result = solver.solve();
        std::cout << "Numar maxim sarituri: " << result.first << "\n";
        std::cout << "Coordonate de pornire: (" << result.second.first << ", " << result.second.second << ")\n";
        break;
    }
    return 0;
}
