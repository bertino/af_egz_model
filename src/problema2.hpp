const int defaultSize = 10;

#include <cstring>

template <typename T> struct Stack {
    int top, capacity;
    T* data;

    Stack()
    {
        data = new T[defaultSize];
        capacity = defaultSize;
        top = 0;
    }

    void push(T item)
    {
        if (top >= capacity) {
            T* newData = new T[capacity += defaultSize];
            std::memcpy(newData, data, capacity * sizeof(T));
            delete[] data;
            data = newData;
        }
        
        data[++top] = item;
    }

    T pop() {
        return top?data[top--]:-1;
    }
};
