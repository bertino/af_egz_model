#include <iostream>
#include <vector>

typedef std::vector<std::vector<int>> matrix;

enum dir { north, east, south, west };

const std::pair<int, int> dirOffsets[4] = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };

class Solver {
private:
    matrix buildings;

public:
    Solver()
    {
        int n, m;
        std::cin >> n >> m;

        buildings = matrix(n);

        for (int i = 0; i < n; i++) {
            std::vector<int> line1(m);
            buildings[i] = std::move(line1);

            for (int j = 0; j < m; j++)
                std::cin >> buildings[i][j];
        }
    }

    int getCost(int row, int col, matrix& visited)
    {
        visited[row][col] = 1;

        int jumpDir = -1;
        int maxHeight = 0;

        for (int i = 0; i < 4; i++) {
            int neighborRow = row + dirOffsets[i].first;
            int neighborCol = col + dirOffsets[i].second;
            if (0 <= neighborRow && neighborRow < buildings.size() && 0 <= neighborCol
                && neighborCol < buildings[0].size()) {
                if (buildings[neighborRow][neighborCol] <= buildings[row][col] && !visited[neighborRow][neighborCol]
                    && buildings[neighborRow][neighborCol] > maxHeight) {

                    maxHeight = buildings[neighborRow][neighborCol];
                    jumpDir = i;
                }
            }
        }

        if (jumpDir == -1) return 0;
        else
            return getCost(row + dirOffsets[jumpDir].first, col + dirOffsets[jumpDir].second, visited) + 1;
    }

    std::pair<int, std::pair<int, int>> solve()
    {
        std::pair<int, int> position;
        int maxJumps = 0;
        int n = buildings.size(), m = buildings[0].size();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix visited = matrix(n, std::vector<int>(m));
                int jumps = getCost(i, j, visited);
                if (jumps > maxJumps) {
                    maxJumps = jumps;
                    position = std::make_pair(i, j);
                }
            }
        }
        return make_pair(maxJumps, position);
    }
};
