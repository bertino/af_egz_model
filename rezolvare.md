---
title: Examen AF
---

# Subiect I

3.  Subprogramul este apelat la infinit (sau până ocupă prea multă memorie), întrucât nu prezintă nicio condiție de oprire a apelării.

4.  Se consideră o matrice de 8 linii, formată doar din elemente de 0 și 1. Matricea are următoarele proprietăți: prima linie conține un singur element cu valoarea 1, linia $j$ conține de două ori mai multe elemente nenule decât linia $j-1$, pentru orice $j=2,...,8$, ultima linie conține un singur element cu valoarea 0. Care este numărul total de elemente cu valoarea 0 din matrice? Justificați.

    Întrucât fiecare linie conține de două ori mai multe elemente decât cea precedentă, linia $j$ conține $2^{j-1}$ elemente nenule. Astfel, pe ultima linie avem $2^{7}$ de $1$ și un $0$, adică $129$ elemente în total.

    Astfel, dimensiunea matricei este 8 linii și 129 coloane.

    Pe fiecare linie $j$ avem $129 - 2^{j-1}$ elemente cu val. 0, astfel că, în total, avem $\sum_{j=1}^{8}(129 - 2^{j-1})$ elemente nule, adică $129*8 - \sum_{j=0}^{7}(2^{j})$, adică $777$

5.  Rezolvare:
    
    ```python
    import sys


    def from_base_10(number: int, base: int):
        result = []
        while number > 0:
            result.append(number % base)
            number = int(number / base)
        result.reverse()
        return result


    def is_magic(x: int, p: int, q: int):
        return set(from_base_10(x, p)) == set(from_base_10(x, q))


    def main(n=0, p=10, q=5):
        magic_numbers = []
        for x in range(1, n):
            if is_magic(x, p, q):
                magic_numbers.append(x)
        print(magic_numbers)


    if __name__ == "__main__":
        if len(sys.argv) < 4:
            print("Usage: problema5.py n p q")
        main(n=int(sys.argv[1]), p=int(sys.argv[2]), q=int(sys.argv[3]))
    ```

6.  Rezolvare:

    ```c++
    #include <iostream>
    #include <vector>

    typedef std::vector<std::vector<int>> matrix;

    enum dir { north, east, south, west };

    void inputDataAndInit(matrix& buildings, matrix& lookup, matrix& visited)
    {
        int n, m;
        std::cin >> n >> m;

        lookup = matrix(n);
        buildings = matrix(n);
        visited = matrix(n);

        for (int i = 0; i < n; i++) {
            std::vector<int> line1(m);
            buildings[i] = std::move(line1);
            std::vector<int> line2(m);
            lookup[i] = std::move(line2);
            std::vector<int> line3(m);
            visited[i] = std::move(line3);

            for (int j = 0; j < m; j++) {
                std::cin >> buildings[i][j];
                lookup[i][j] = -1;
                visited[i][j] = 0;
            }
        }
    }

    int getCost(int row, int col, const matrix& buildings, matrix& lookup, matrix& visited)
    {
        if (lookup[row][col] != -1) return lookup[row][col];
        visited[row][col] = 1;
        int jumpDir = -1;
        int maxHeight = 0;
        std::vector<std::pair<int, int>> dirOffsets = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };

        for (int i = 0; i < 4; i++) {
            int neighborRow = row + dirOffsets[i].first;
            int neighborCol = col + dirOffsets[i].second;
            if (0 <= neighborRow && neighborRow < buildings.size() && 0 <= neighborCol && neighborCol < buildings[0].size())
                if (buildings[neighborRow][neighborCol] <= buildings[row][col] && !visited[neighborRow][neighborCol]
                    && buildings[neighborRow][neighborCol] > maxHeight) {

                    maxHeight = buildings[neighborRow][neighborCol];
                    jumpDir = i;
                }
        }

        if (jumpDir == -1) lookup[row][col] = 0;
        else
            lookup[row][col]
                = getCost(row + dirOffsets[jumpDir].first, col + dirOffsets[jumpDir].second, buildings, lookup, visited)
                + 1;
        return lookup[row][col];
    }

    std::pair<int, int> solve(const matrix& buildings, matrix& lookup, matrix& visited) {
        std::pair<int, int> result;
        int maxJumps = 0;
        for (int i=0; i<buildings.size(); i++) {
            for (int j=0; j<buildings[0].size(); j++) {
                if (getCost(i, j, buildings, lookup, visited) > maxJumps) {
                    maxJumps = getCost(i, j, buildings, lookup, visited);
                    result = std::make_pair(i, j);
                }
            }
        }
        return result;
    }

    int main()
    {
        matrix buildings, lookup, visited;
        inputDataAndInit(buildings, lookup, visited);

        auto result = solve(buildings, lookup, visited);
        std::cout << "(" << result.first << ", " << result.second << ")\n";

        return 0;
    }
    ```
